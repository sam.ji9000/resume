Sancho Muñoz Jimenez 
===========

Email: [sam.ji9000@gmail.com](sam.ji9000@gmail.com)   
LinkedIn: [My Linkedin](https://www.linkedin.com/in/samji9000/)  
Location: Madrid, Spain  

Hard skills
-----------
- Infrastructure as code
- Design of web API using swagger, applying REST and API first principles
- Microservices and Event Sourcing architectures
- Message queues: Rabbit, NATS
- Streaming systems: Apache Kafka, NATS streaming
- Programming patterns: Listen to yourself, Observer, Singleton, MVC, DRY, SRP, ACID, CAP, ATDD
- Concurrency control
- High availability and critical systems
- Alerting and observability
- Blue green deployments with zero downtime
- Inversion of Control/Dependency Injection
- Logging as a Service
- Automated testing, unit testing, E2E testing
- Docker, Docker-compose, container systems
- Git VCS 
- Devops culture and developer tooling with custom project template generators and scripts. 
- Basic knowledge of frontend architectures and technologies: React, Vue
- UNIX systems knowledge: Ubuntu, Debian and it's server distributions 
- PaaS systems: AWS, Openshift
- AWS technologies: Fargate, EKS, ECS, AuroraDB, Atlas, Route53
- Security notions: https, auth protocols, certificates, JWT, API governance, identity providers, SSL, TLS
- NodeJS
- Golang
- Flink
- Javascript
- Java
- Python, Django, DRF, Celery, SQLAlchemy
- PHP
- Databases: SQL, MySQL, Oracle, PostgreSQL, MongoDB
- Redis
- Salesforce's APEX
- IoT
- System administration
- AI integration in existing systems to automate and streamline processes
- Data pipelines to train AI models
- Kubernetes
- Helm
- Skaffold 

Soft skills
-----------
- Agile and SCRUM principles and methodologies
- Ability to translate complex initiatives into small and understandable tasks for the development team
- Communication skills both within the organizations I've worked for and their customers
- Curious and adaptable
- Conflict solving skills
- Skills for hiring processes as interviewer
- Helping junior team members grow, with learning paths, teaching and counseling

Education
---------

### (Jun 2016)**University of Extremadura**
Bachelor in Software Engineering

Work Experience
---------------
### (Jun 2024 - Now) **Fever: Senior Software engineer**
[Fever](https://feverup.com/en) is a company that democratizes access to culture and entertainment

Working in Fever I use frameworks as Django and Flask and tools like Datadog or Kafka.
I'm responsible for the backend portion of my team leading a backend team of 4 members. I ensure the tasks are scheduled to fulfill deadlines and are refined as much as possible, speeding up the development time of new features and changes.
I'm also in charge of the alerting system for my team, based in Datadog + Slack integration to ensure we get notified of any critical error happening in our system. 
In addition to that I'm helping migrating workers from Celery to Kafka for async processing.
Last I'm usually in charge of operatives that involve some of Fever's most important partners, ensuring customer satisfaction and streamlining communications with them to keep them on the loop.


### (Apr 2022 - Dec 2023) **Auvik: Technical Lead**
[Auvik](https://www.auvik.com/) is a SaaS network management company based in Ontario, Canada.

When I joined there was an initiative to transition from a monolith architecture to a microservices one. One of the tools used for that purpose was Apache Flink, a framework for stateful computations over unbounded and bounded data streams. I developed with [giter8](https://github.com/foundweekends/giter8) a template to easily create new Flink Jobs, automating the tests and template generation to ensure it works after every new change. I also did a very thorough research on FlinkSQL, an ANSI standard compliant SQL engine developed for Flink that can process both real-time and historical data so it could be easily used in some simpler parts of the data pipeline. 

I led two teams. 
One of them was responsible for delivering new monitoring features using the existing monolith and a custom programming language developed by Auvik some years ago. We successfully delivered integrations that would improve the monitoring capabilities for vendors like Juniper Mist or Cisco. At the same time we maintained a part of the existing polling system that was responsible for gathering network device data for monitoring.
The other team was focused on developing new tools for the other teams, specially the one mentioned first, based on the new microservices architecture. 

We also worked very closely with several other teams in the company. I made sure to consult every decision we made with them, to ensure the frictionless integration of every change we made, also being open to any feedback or improvement they would require to make it easier and faster to deliver new features to the customer and improve our code quality overall. 


### (Oct 2021 - Mar 2022) **Circular: Senior Backend Engineer**
[Circular](https://circular.io/) is a hiring platform, built in Django.

I helped maintain and develop the platform. The platform was transitioning from Django to DRF(Django Rest Framework). My team was focused on features for the recruiters that use the platform.
We migrated the recruiter's homepage and all the significant tabs and functionality related to it from Django to DRF + React, I was responsible for all the backend in that area. During this migration I studied the legacy Django views to extract the 
required information and translate it to REST endpoint, also pruning any unnecessary operation from there and improving the efficiency of the code to make it faster. The new homepage was released and worked as expected, having a good reception by the users.

Also I developed automation processes for the platform. One of my last automation projects was related with a SaaS community called [Circle](https://circle.so/) which was used by the recruiters to communicate between them. I automated all the synchronization between Circle and Circular: creating accounts for the recruiters once their account is approved, integrating our SSO provider with Circle, synchronizing the profile data between both platforms and linking the reputation system used in Circular so it's displayed on each account in Circle. This automation process achieved the goal of making the navigation and synchronization seamless between the two platforms.

### (Dec 2017 - Sept 2021) **Intelygenz(Spain)/Intelygenz INC(US)**

#### **Software developer (2017-2018)**
My first project with [Intelygenz](https://intelygenz.com/) was for BMW and it consisted in a backoffice for garages to enable them to compose, configure and send notifications to their customers related to their vehicle leases. The project was developed using Java with a custom API framework by Intelygenz, Oracle, and AngularJS in the frontend. Processes were mostly related with customer interaction with the databases and scheduling cronjobs to send the emails to the customer.

#### **Technical lead (2018-2019)**

My second project with Intelygenz was for the telco [Orange](https://www.orange.es/). The project consisted in implementing automated provisioning processes of services and devices. Since the project involved other teams, companies and systems, we used Apache Kafka to create an event driven system to communicate the different services between them. Also, we implemented a microservice architecture in order to make it easier and more efficient to escalate the system in case some services had more workload than others. We were in charge of defining and implementing E2E processes that involved several services/vendors. For this project me and my team used mainly Nodejs, Apache Kafka, MongoDB, Redis, Openshift, Docker, Terraform , Salesforce (using APEX to connect to them). We also automated all the pipelines, having not only builds but linters, unit test and integration test on them.

#### **Software Architect, Technical Lead (2019-2021)**

After that I was moved to the US to work with Intelygenz INC, the US branch of the company, located in San Francisco. Me and my team started from scratch an IPA (intelligent process automation) project for the telco [Mettel](https://www.mettel.net/). The project was a cluster of microservices that automate several processes related with Mettel's business logic like ticket processing or real time device monitoring among others. The architecture was designed to communicate with several vendors and Mettel systems and orchestrate processes that involve consulting or taking actions in those systems fast enough to comply with project SLAs. Some of the technologies used: Python 3.6, NATS message queue, Docker, Redis, Salesforce(using APEX to connect to them), AWS's Fargate, Gitlab pipelines and Terraform. All pipelines were fully automated with linters, test, build and deploy stages.
This project had an integration with an AI model developed also by Intelygenz. From our side we communicated with the AI's API to generate information about open tickets. We made this information available in each ticket, drastically reducing the amount of time required to triage it. This Automation + AI project won an [award](https://www.prnewswire.com/news-releases/mettel-wins-2019-american-business-award-for-helping-enterprises-map-to-future-trends-technologies-at-mettel-innovation-summit-300849600.html) during that time Mettel was one of the leaders in the Gartner Magic Quadrant for
Managed Mobility Services.

Aside from the technical management of the team I also was involved in all the daily meetings with the customer. There I helped the customer and the manager with the technical scope and initial estimation of each technical task, also breaking in smaller tasks any requirement that was big enough to make the estimation and development harder than it should.

Also I was involved in the hiring process of any python candidate, writing and evaluating technical tests and interviewing the candidates in the technical step of the hiring process.

### (Oct 2016 - Dec 2017) **Tecnilogica/Accenture(PosterDigital): Fullstack software engineer, Android developer, R&D**
_Tecnilogica was bought by Accenture after I joined them_

As Android developer I manage the android system for the digital signage
product [PosterDigital](https://posterdigital.com/es/). My tasks were related to: code refactoring, optimization,
new features, maintenance, and solving issues. The android system used
technologies as: retrofit, cordova-crosswalk and ion. Apart from that, some of the features were related with working at a low level with specific devices in order to use device native features such as lights, system schedulers and advanced device configuration.

As Full-Stack developer I helped with the CMS service of PosterDigital,
developing new features, maintaining the existing code, profiling video
content, PoCs, and basic sysadmin tasks related with monitoring and release of features. For that purposes I use technologies as
Javascript, CSS, PHP, MySQL, Apache, Linux (Ubuntu), FFmpeg and
RabbitMQ among others. 

I developed some R&D projects, like a system based in LeapMotion to control the navigation flow of webapps and HTML based presentations.

With Accenture/Tecnilogica I've worked for customers such as [3M](https://www.3m.com/), [VIPS](https://www.vips.es/) and [3INA](https://3ina.com/) giving support 24/7 and developing projects across all the globe.

### (Feb 2014 - Jun 2016) **University of Extremadura: Researcher, Course teacher**
As a research associate I worked for both [GIM](http://gim.unex.es/) and [Robolab](https://robolab.unex.es/). 
My research is related to game development, virtual reality (VR) and augmented reality (AR) and IoT, using tools as SFML and Unity3D. 
Along with the research I teached in some University courses, related with game design, game development and Unity.

### (Jan 2014 - April 2016) **Freelance**
We were a team of 3 developers. My job inside the team was management of the tasks to be performed by the team and the deadlines of those tasks, talks with the customers, coding and sound design assistant. We used Unity3D to develop. The most remarkable projects are a mobile game for soccer and a VR demo consisting of 3 different 3D levels of an Action RPG game.

Projects
-----------------
In my free time I like to write stories for tabletop role playing games or LARPGs and play those with my friends. I also like game development and I'm always with a personal project related to that in mind. Some of my projects are: developing a mod for a game and administrating the server I have for it. I've also collaborated with other projects like [Legends of STEM](https://github.com/legendsOfSTEM/Game) and I'm part of a group of game devs called "The Guard Studio"
